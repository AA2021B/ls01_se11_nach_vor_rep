﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag = 0;
       double rückgabebetrag;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen (tastatur);
       // Geldeinwurf
       // -----------
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag,tastatur);
       
       // Fahrscheinausgabe
       Fahrscheinausgabe();
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       ruegabebetrag(rückgabebetrag);
       

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    
	}
	private static void ruegabebetrag(double rückgabebetrag) {
		
		
	}
	public static float  fahrkartenbestellungErfassen(Scanner tastatur) {
		int anzahlTickets;
		float ticketpreis;
		
		System.out.print("Zu zahlender Betrag (EURO): ");
	       ticketpreis = tastatur.nextFloat();
	       
	    System.out.print("Wie viele Tickets? ");
	       anzahlTickets = tastatur.nextInt();

		return  ticketpreis * anzahlTickets ;
    }
	public static double fahrkartenBezahlen (float zuZahlenderBetrag, Scanner tastatur) {
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze= 0.0;
		 
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f\n " , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextFloat();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	       return zuZahlenderBetrag - eingezahlterGesamtbetrag;
	}
	public static void Fahrscheinausgabe() 
	{System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    System.out.println("\n\n");}
	
	public static void rückgabebetrag(double rückgabebetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		double zuZahlenderBetrag = 0.0;
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f\n "  , rückgabebetrag , " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	}
	}