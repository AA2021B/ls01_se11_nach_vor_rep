import java.util.Scanner;
public class Arrays {

	public static void main(String[] args) {
		
	Scanner sc = new Scanner (System.in);
	
	//Deklaration eines Arrays, 5 integer Werte
	//Name des Arrays: "intArray"
	
	int[] intArray = new int [5];
	System.out.println("Geben Sie ein wert ein");
	intArray[4] = sc.nextInt();
	
	//Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
	
	intArray[2] = 1000;
	intArray[4] = 500;
	System.out.println(intArray[2]);
	intArray[4] = 500;
	
	//Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten 
	
	double [] doubleArray = {2.5, 6.4, 9.7};
	
	//Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
	System.out.println(doubleArray[1]);
	//Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
	
	
    System.out.println("An welchen Index soll der neue Wert?");
    int index = sc.nextInt();
    System.out.println("Geben Sie den neuen Wert f�r index " +  index  + " an:");
    doubleArray[index] = sc.nextDouble();
    
  //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // _0_   _0_  _1000_  _500_  _0_  
    for( index = 5; index< 5; index++) {
    	System.out.println(intArray[index]);
    }
    //Der intArray soll mit neuen Werten gef�llt werden
    //1. alle Felder sollen den Wert 0 erhalten
    	for( index = 5; index< 5; index++) {
    		intArray[index] = 0;
    		System.out.println(intArray[index]+ "");
    	}
    //Der intArray soll mit neuen Werten gef�llt werden
    //2. alle Felder sollen vom Nutzer einen Wert bekommen
    //nutzen Sie dieses Mal die Funktion �length�
    	for( index = 5; index<intArray.length; index++) {
    		intArray[index] = 0;
    		System.out.println("position " + index);
    		intArray[index] = sc.nextInt(); 	
    }
    //Der intArray soll mit neuen Werten gef�llt werden
    //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    	for( index = 5; index<intArray.length; index++) {
    		//intArray[index] = (index + 1)*10;
    		intArray[index] = (index + 10);
    		System.out.println(intArray[index]);
    		intArray[index] = sc.nextInt(); 	
    }
	
	}
}
