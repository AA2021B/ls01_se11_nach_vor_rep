
import java.util.Scanner;

class Methoden {
	
    static Scanner sc = new Scanner(System.in); 

    public static void main(String[] args) { //Methodenkopf
    	
    	//Public - Zugriffsmodifizierer
    	//Void - kein R�ckgabedatentyp (leer)
    	//main - Methodenname
    	
        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        programmhinweis (); //Methodenaufruf
        
        zahl1 = eingabe (1);
        zahl2 = eingabe (2);

   
        erg = verarbeitung (erg, zahl1, zahl2); //Argument
        
        ausgabe (zahl1, zahl2 ,erg);
      //Zeile 8 bis 25 Methodendefinition
        erg = multi (zahl1, zahl2);
        ausgabeprodukt(erg, zahl1, zahl2);
    
    }


	public static void programmhinweis() { //Methodenbezeichner/Methodenname
		System.out.println("Hinweis: ");
		System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");}
								//Parameter
	public static void ausgabe(double zahl1, double zahl2, double erg) {
		System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f  + %.2f", zahl1, zahl2, erg);
    }
	 
	public static double verarbeitung (double erg, double zahl1, double zahl2) { //klammer �ffnet den Methodenstrumpf
		erg = zahl1 + zahl2;
		return erg;
	} //Klammer schlie�t den Methodenstrumpf
								//Lokale variablen
	public static double eingabe(int z) {
		Scanner sc = new Scanner(System.in);
		double zahl;
		
		System.out.println(z + "zahl:");
		zahl=sc.nextDouble();
		return zahl;		 // return - R�ckgabewert
		
		
	}
	public static double multi(double faktor1, double faktor2) {
		double produkt;
		produkt = faktor1 * faktor2;
		return produkt;
	}
	
	public static void ausgabeprodukt(double erg, double zahl1, double zahl2) {
        System.out.printf("%.2f = %.2f  * %.2f", erg, zahl1, zahl2);
        System.out.println("\nErgebnis der Multibilaktion");
    }
	
	
	
	
	
	
	
}