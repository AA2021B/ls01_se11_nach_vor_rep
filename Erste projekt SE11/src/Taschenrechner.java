import java.util.Scanner; //Import der klasse Scanner

public class Taschenrechner {

	public static void main(String[] args) {
			
		//Neues Scanner-Objekte myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		
		//Die Variablen zahl1 speichert die erste Eingabe
		long zahl1 = myScanner.nextLong();
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		
		//Die Variablen zahl2 speichert die zweite Eingabe
		long zahl2 = myScanner.nextLong();
		
		long ergebnis = zahl1 * zahl2;
		
		System.out.print("\n\n\nErgebnis der Multipliaktion lautet: ");
		System.out.print(zahl1 + "*" + zahl2 + " = 5" + ergebnis);
		
		 
		
	}

}
