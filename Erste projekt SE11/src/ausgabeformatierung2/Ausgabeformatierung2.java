package ausgabeformatierung2;

public class Ausgabeformatierung2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Der unterschied zwischen print und println besteht darin, dass die print-Methode die Zeichenfolge druckt,den Cursor jedoch nicht in eine neue Zeile bewegt, w�hrend die println-Methode die Zeichenfolge druckt und den Cursor in eine neue Zeile bewegt.
			//Aufgabe 2
			System.out.println("\n");
			
			System.out.printf("%20s\n" ,"*");
			System.out.printf("%21s\n", "***");
			System.out.printf("%22s\n", "*****");
			System.out.printf("%23s\n", "*******");
			System.out.printf("%24s\n", "*********");
			System.out.printf("%25s\n", "***********");
			System.out.printf("%26s\n", "*************");
			System.out.printf("%21s\n", "***");
			System.out.printf("%21s\n", "***");
			 
	  //Aufgabe
			double d = 22.4234;
			System.out.printf("%.2f\n", d);
			double d2 = 111.2222;
			System.out.printf("%.2f\n", d2);
			double d3 = 4.0;
			System.out.printf("%.2f\n", d3);
			double d4  = 1000000.551;
			System.out.printf("%.2f\n", d4);
			double d5 = 97.34;
			System.out.printf("%.2f\n", d5);
			System.out.println();
			
			//�bung 2 Aufgabe 1
			System.out.println("Aufgabe 2 ");
			System.out.printf("%20s\n","**");
			System.out.printf("%16s","*");
			System.out.printf("%7s\n", "*");
			System.out.printf("%16s","*");
			System.out.printf("%7s\n","*");
			System.out.printf("%20s\n","**");
		
			//Aufgabe 3
			System.out.println("Aufgabe3\n");
			
			System.out.printf("%s%3s%10s\n", "Fahrenheit" , "|" , "Celsuis");
			System.out.println("------------------------");
			System.out.printf("%s%10s%10s\n", "-20" , "|" , "-28.89");
			System.out.printf("%s%10s%10s\n", "-10" , "|" , "-23.33");
			System.out.printf("%s%11s%10s\n", "+0" , "|" , "-17.78");
			System.out.printf("%s%10s%10s\n", "+20" , "|" , "-6.67");
			System.out.printf("%s%10s%10s\n", "+30" , "|" , "-1.11");
	}

}
